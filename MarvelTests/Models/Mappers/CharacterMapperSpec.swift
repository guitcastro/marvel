//
//  CharacterMapperSpec.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ObjectMapper

@testable import Marvel

class CharacterMapperSpec : QuickSpec {
    
    override func spec() {
        
        let subject = Mapper<MarvelCharacter>()
        var character: MarvelCharacter?
        
        describe("The Mapper") {
            context("Success JSON") {
                beforeEach {
                    character = subject.map(["id" : 1,
                        "name": "test",
                        "thumbnail":
                            ["path": "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                                "extension": "jpg"],
                        "description" : "some descrition",
                        "comics" : ["collectionURI": "http://gateway.marvel.com/v1/public/characters/1011334/comics"],
                        "events" : ["collectionURI": "http://gateway.marvel.com/v1/public/characters/1011334/events"],
                        "series" : ["collectionURI": "http://gateway.marvel.com/v1/public/characters/1011334/series"],
                        "stories" : ["collectionURI": "http://gateway.marvel.com/v1/public/characters/1011334/stories"],
                        ])
                }
                it("Map required properties with success") {
                    expect(character?.id).to(equal(1))
                    expect(character?.name).to(equal("test"))
                }
                it("Map optional properties with success") {
                    expect(character?.characterDescription).to(equal("some descrition"))
                    expect(character?.thumbnail).to(equal("http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg"))
                    expect(character?.comicsUrl).to(equal("http://gateway.marvel.com/v1/public/characters/1011334/comics"))
                    expect(character?.eventsUrl).to(equal("http://gateway.marvel.com/v1/public/characters/1011334/events"))
                    expect(character?.seriesUrl).to(equal("http://gateway.marvel.com/v1/public/characters/1011334/series"))
                    expect(character?.storiesUrl).to(equal("http://gateway.marvel.com/v1/public/characters/1011334/stories"))
                }
            }
            context("invalid Json") {
                beforeEach {
                    character = subject.map(["name": "test"])
                }
                it("fail to map with no Id is specify") {
                    expect(character).to(beNil())
                }
            }
        }
    }
}