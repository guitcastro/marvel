//
//  MarvelApiSpec.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import Quick
import Nimble
import OHHTTPStubs

@testable import Marvel

class MarvelApiClientSpec : QuickSpec {
    
    private func mockSuccessCharactersRequest() {
        stub(isPath("/v1/public/characters")) { _ in
            let fixture = OHPathForFile("characters.json", self.dynamicType)
            return OHHTTPStubsResponse(fileAtPath: fixture!,
                                       statusCode: 200, headers: ["Content-Type":"application/json"])
        }        
    }
    
    private func mockErrorCharactersRequest() {
        stub(isPath("/v1/public/characters")) { _ in
            let fixture = OHPathForFile("characters.json", self.dynamicType)
            return OHHTTPStubsResponse(fileAtPath: fixture!,
                                       statusCode: 500, headers: ["Content-Type":"application/json"])
        }
    }
    
    private func mockInvalidCharacterJson() {
        stub(isPath("/v1/public/characters")) { _ in
            let stubData = "{ \"noId\" : 1, \"noName\" : \"test\" }".dataUsingEncoding(NSUTF8StringEncoding)
            return OHHTTPStubsResponse(data: stubData!, statusCode:200, headers: ["Content-Type":"application/json"])
        }
    }
    
    override func spec() {
        
        var subject: MarvelApiClient?
        
        describe("The api Client") {
        
            beforeEach({ 
                subject = AlamofireMarvelApiClient(apiKey: "any")
            })
            
            context("The characters resquest") {
                
                context("Request is executed with success") {
                    var characters:[MarvelCharacter]? = nil
                    
                    beforeEach {
                        self.mockSuccessCharactersRequest()
                        characters = nil
                        subject?.getCharacters(0, successBlock: { (chars) in
                            characters = chars
                            }, failBlock: { error in print(error) })
                        expect(characters).toEventually(beTruthy())
                    }
                    
                    it ("Return a list of Characters") {
                        expect(characters).notTo(beEmpty())
                    }
                    
                }
                
                context("The request fail") {
                    
                    var didCallSuccessBlock = false
                    var didCallFailBlock = false
                    
                    it("Fail with request error") {
                        self.mockErrorCharactersRequest()
                        subject?.getCharacters(0, successBlock: { (_) in
                            didCallSuccessBlock = true
                            }, failBlock: { error in
                                didCallFailBlock = true
                                expect(error).to(equal(ClientError.RequestError))
                        })
                        expect(didCallFailBlock).toEventually(beTruthy())
                        expect(didCallSuccessBlock).to(beFalse())
                    }
                    
                    it("Fail to parse json") {
                        self.mockInvalidCharacterJson()
                        subject?.getCharacters(0, successBlock: { (_) in
                            didCallSuccessBlock = true
                            }, failBlock: { error in
                                didCallFailBlock = true
                                expect(error).to(equal(ClientError.FailToParseJsonResponse))
                        })
                        expect(didCallFailBlock).toEventually(beTruthy())
                        expect(didCallSuccessBlock).to(beFalse())
                    }
                    
                }
                
            }
            
        }
    }
}