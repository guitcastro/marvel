//
//  HQResourceType.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

enum HQResourceType: String {
    case Comics = "Comics"
    case Series = "Series"
    case Stories = "Stories"
    case Events = "Events"
}