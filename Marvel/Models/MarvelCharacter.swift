//
//  Characters.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

final class MarvelCharacter {
    
    let id:  Int
    let name: String
    let thumbnail: String?
    
    var characterDescription: String?
    
    var comicsUrl: String?
    var seriesUrl: String?
    var storiesUrl: String?
    var eventsUrl: String?
    
    init (id: Int, name: String, thumbnail: String?) {
        self.id = id
        self.name = name
        self.thumbnail = thumbnail
    }
    
}