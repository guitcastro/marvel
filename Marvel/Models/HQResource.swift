//
//  HQResource.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

final class HQResource {
    let title:String
    let thumbnail: String?
    
    init (title: String, thumbnail: String?) {
        self.title = title
        self.thumbnail = thumbnail
    }
}