//
//  CharacterMapper.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import ObjectMapper

extension MarvelCharacter : Mappable {
    
    convenience init?(_ map: Map) {
        let id:Int = map["id"].valueOrFail()
        let name:String = map["name"].valueOrFail()
        
        var thumbnail:String? = nil
        
        if let path:String = map["thumbnail.path"].value() {
            if let thumbnailExtension:String = map["thumbnail.extension"].value() {
                thumbnail = path + "." + thumbnailExtension
            }
        }
        
        if map.isValid {
            self.init(id: id, name: name, thumbnail: thumbnail);
        } else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        self.comicsUrl = map["comics.collectionURI"].value()
        self.eventsUrl = map["events.collectionURI"].value()
        self.seriesUrl = map["series.collectionURI"].value()
        self.storiesUrl = map["stories.collectionURI"].value()
        self.characterDescription = map["description"].value()
    }
}