//
//  HQResourceMapper.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

import Foundation
import ObjectMapper

extension HQResource : Mappable {
    
    convenience init?(_ map: Map) {
        let title:String = map["title"].valueOrFail()
        
        var thumbnail:String? = nil
        
        if let path:String = map["thumbnail.path"].value() {
            if let thumbnailExtension:String = map["thumbnail.extension"].value() {
                thumbnail = path + "." + thumbnailExtension
            }
        }
        
        if map.isValid {
            self.init(title: title, thumbnail: thumbnail);
        } else {
            return nil
        }
    }
    
    
    func mapping(map: Map) {
        
    }
    
}