//
//  Segues.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

class Segues {
    static let ComicsSegue = "ComicsSegue"
    static let StoriesSegue = "StoriesSegue"
    static let EventsSegue = "EventsSegue"
    static let SeriesSegue = "SeriesSegue"
    static let DetailsControllerSegue = "DetailsViewController"
    static let MarvelCarouselViewControllerSegue = "MarvelCarouselViewController"
}