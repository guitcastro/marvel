//
//  ComicBookView.swift
//  Marvel
//
//  Created by Guilherme on 5/17/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit

protocol ComicBookViewProtocol  {
    var hqImageView: UIImageView! { get }
    var title: UILabel! { get }
    var loadingIndicator: UIActivityIndicatorView! { get }
}

extension ComicBookViewProtocol {
    func bind(hq: HQResource) {
        
        self.loadingIndicator.startAnimating()
        self.title.text = hq.title
        
        self.hqImageView.downloadAndShow(hq.thumbnail)
    }

}

class ComicBookView : UIView, ComicBookViewProtocol {
    
    @IBOutlet weak var hqImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
}