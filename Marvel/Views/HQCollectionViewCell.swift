//
//  HQCollectionViewCell.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import AlamofireImage
import UIKit


class HQCollectionViewCell : UICollectionViewCell, ComicBookViewProtocol {
    
    @IBOutlet weak var hqImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
}