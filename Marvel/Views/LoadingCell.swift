//
//  LoadingCell.swift
//  Marvel
//
//  Created by Guilherme on 5/14/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell : UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
