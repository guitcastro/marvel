//
//  MarvelCharacterCell.swift
//  Marvel
//
//  Created by Guilherme on 5/14/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit

class MarvelCharacterCell : UITableViewCell {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    func bind(hero: MarvelCharacter) {
        self.backgroundImage.hidden = true
        self.spinner.startAnimating()
        self.name.text = hero.name
        
        self.backgroundImage?.downloadAndShow(hero.thumbnail)
        
    }

}