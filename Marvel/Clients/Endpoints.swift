//
//  Endpoints.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation

class Endpoints {
    static let base = "https://gateway.marvel.com/v1/public/"
    static let characters = base + "characters"
}