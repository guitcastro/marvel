//
//  MarvelApiClient.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation


protocol MarvelApiClient {
    
    init (apiKey: String)
    func getCharacters(offset: Int,
                       successBlock:([MarvelCharacter] -> Void),
                       failBlock: (ClientError -> Void))
    
    func filterCharacters(nameStartsWith: String,
                          successBlock:([MarvelCharacter] -> Void),
                          failBlock: (ClientError -> Void))
    
    func getHQResource(resourceUrl: String,
                       successBlock:([HQResource] -> Void),
                       failBlock: (ClientError -> Void))
    
}

