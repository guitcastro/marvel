//
//  AlamofireMarvelApiClient.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper

enum ClientError : ErrorType {
    case RequestError
    case FailToParseJsonResponse
}

class  AlamofireMarvelApiClient : MarvelApiClient {
    
    let apiKey: String
    private let privateApiKey = "b6c08ae3695afa6e77f53ae19cec5d6e1def28c2"

    
    required init (apiKey: String) {
        self.apiKey = apiKey
    }
    
    private func getHash(ts: String) -> String {
        return (ts + self.privateApiKey + self.apiKey).md5
    }
    
    private func executeResquest(endpoint: String,
                                 parameters: [String: AnyObject] = [String: AnyObject](),
                                 successBlock: (JSON -> Void),
                                 failBlock: (ClientError -> Void)) {
        let timeStamp = String(NSDate().timeIntervalSinceNow)
        let hash = getHash(timeStamp)
        var mutableParameters = parameters
        mutableParameters["ts"] = timeStamp
        mutableParameters["hash"] = hash
        mutableParameters["apikey"] = self.apiKey
        
        Alamofire.request(.GET, endpoint, parameters: mutableParameters)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch (response.result) {
                case .Failure( _):
                    failBlock(ClientError.RequestError)
                case .Success(let json):
                    let swiftJson = JSON(json)
                    let results = swiftJson["data"]["results"]
                    successBlock(results)
            }
        }
        
    }
    
    // MARK: - MarvelApiClient
    
    func getCharacters(offset: Int,
                       successBlock:([MarvelCharacter] -> Void),
                       failBlock: (ClientError -> Void)) {
        let parameters = ["offset" : offset]
        self.executeResquest(Endpoints.characters, parameters: parameters, successBlock: { json in
            let array = json.arrayObject
            if let characters = Mapper<MarvelCharacter>().mapArray(array) {
                successBlock(characters)
            } else {
                failBlock(ClientError.FailToParseJsonResponse)
            }
        }, failBlock: failBlock)
    }
    
    func filterCharacters(nameStartsWith: String,
                       successBlock:([MarvelCharacter] -> Void),
                       failBlock: (ClientError -> Void)) {
        let parameters = ["nameStartsWith" : nameStartsWith]
        self.executeResquest(Endpoints.characters, parameters: parameters, successBlock: { json in
            let array = json.arrayObject
            if let characters = Mapper<MarvelCharacter>().mapArray(array) {
                successBlock(characters)
            } else {
                failBlock(ClientError.FailToParseJsonResponse)
            }
            }, failBlock: failBlock)
    }
    
    func getHQResource(resourceUrl: String,
                       successBlock:([HQResource] -> Void),
                       failBlock: (ClientError -> Void)) {
        self.executeResquest(resourceUrl, successBlock: { json in
            let array = json.arrayObject
            if let characters = Mapper<HQResource>().mapArray(array) {
                successBlock(characters)
            } else {
                failBlock(ClientError.FailToParseJsonResponse)
            }
            }, failBlock: failBlock)
    }
    
    
}

