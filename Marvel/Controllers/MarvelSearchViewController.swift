//
//  MarvelSearchViewController.swift
//  Marvel
//
//  Created by Guilherme on 5/14/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class MarvelSearchViewController: ViewController , UISearchResultsUpdating, UISearchBarDelegate {
    
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSearchBar()
    }
    
    override func getCharacters() {}

    
    private func configureSearchBar() {
        
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false
        
        self.searchController.hidesNavigationBarDuringPresentation = false;
        self.searchController.searchBar.searchBarStyle = .Minimal;
        self.navigationItem.titleView = self.searchController.searchBar;
        
        let textFieldInsideSearchBar =  self.searchController.searchBar.valueForKey("searchField") as? UITextField        
        textFieldInsideSearchBar?.textColor = Colors.MarvelRed
        
        self.definesPresentationContext = true;
        
    }
    
    // MARK: - TableView
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }    
    
    // MARK: UISearchBarDelegate
    
    private func filterResults(text:String){
        if text.isEmpty {
            self.characters = []
            self.tableView.reloadData()
        }
        self.client.filterCharacters(text, successBlock: { (characters) in
            self.characters = characters
            self.tableView.reloadData()
        }) { (error) in
            
        }
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterResults(searchController.searchBar.text!)
        
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.filterResults(searchBar.text!)
    }
    
    
}
