//
//  MarvelCollectionViewController.swift
//  Marvel
//
//  Created by Guilherme on 5/15/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit

class  MarvelCollectionViewController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var hqTypeLabel: UILabel!
    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var hqResourceType: HQResourceType!
    var hqResourceUrl: String?
    
    var client: MarvelApiClient!
    
    var hqResources = [HQResource]()
    
    override func viewDidLoad() {
        
        self.hqTypeLabel.text = self.hqResourceType.rawValue
        self.client = AlamofireMarvelApiClient(apiKey: "b724564ddc1f937dd41175d8cdc5d825")
                
        if let url = self.hqResourceUrl {
            self.client.getHQResource(url, successBlock: { hqResources in
                if hqResources.count > 0 {
                    self.hqResources = hqResources
                    self.collectionView.reloadData()
                } else {
                    self.errorButton.hidden = false
                }
                self.activityIndicator.hidden = true
                }, failBlock: { (error) in
                    self.activityIndicator.hidden = true
                    self.errorButton.hidden = false
            })
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? MarvelCarouselViewController {
            controller.items = self.hqResources
        }
    }
    
    @IBAction func didTouchErrorButton(sender: AnyObject) {
    }
    // MARK : - UICollectionViewDataSource

    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hqResources.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("hqCell", forIndexPath: indexPath) as! HQCollectionViewCell
        cell.bind(hqResources[indexPath.row])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.performSegueWithIdentifier(Segues.MarvelCarouselViewControllerSegue, sender: self)
    }
    
}