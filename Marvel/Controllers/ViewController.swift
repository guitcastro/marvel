//
//  ViewController.swift
//  Marvel
//
//  Created by Guilherme Castro on 5/6/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var client: MarvelApiClient!
    var characters = [MarvelCharacter]()
    var selectedCharacter: MarvelCharacter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .Black;
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "icn-nav-marvel"))

        
        self.tableView.backgroundView = UIView()
        self.tableView.backgroundColor = Colors.MarvelDarkNavigationBarColor
        
        self.client = AlamofireMarvelApiClient(apiKey: "b724564ddc1f937dd41175d8cdc5d825")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? DetailsViewController {
            controller.character = self.selectedCharacter!
        }
    }
    
    func getCharacters() {
        self.client.getCharacters(self.characters.count, successBlock: { (characters) in
            self.characters.appendContentsOf(characters)
            self.tableView.reloadData()
        }) { (error) in
            
        }
    }
    
    // MARK: - TableView
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row >= self.characters.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("LoadingCell") as! LoadingCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        
        let tableViewCell = tableView.dequeueReusableCellWithIdentifier("MarvelCharacterTableViewCell") as? MarvelCharacterCell
        tableViewCell?.bind(characters[indexPath.row])
        return tableViewCell!
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.characters.count {
            self.getCharacters()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedCharacter = self.characters[indexPath.row]
        self.performSegueWithIdentifier(Segues.DetailsControllerSegue, sender: self)
    }
    
}

