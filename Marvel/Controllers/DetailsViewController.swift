//
//  DetailsViewController.swift
//  Marvel
//
//  Created by Guilherme on 5/14/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController , UIScrollViewDelegate {
    
    var character: MarvelCharacter!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.text = self.character.name
        self.image.downloadAndShow(self.character.thumbnail)
        self.descriptionLabel.text = self.character.characterDescription?.isEmpty == false ? self.character.characterDescription : "N/A"
        self.scrollView.delegate = self
        
    }
    
       override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.alpha = 0.7
        self.navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.alpha = 1.0
        self.navigationController?.hidesBarsOnSwipe = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller = segue.destinationViewController as! MarvelCollectionViewController
        switch segue.identifier {
        case .Some(Segues.ComicsSegue):
            controller.hqResourceType = HQResourceType.Comics
            controller.hqResourceUrl = self.character.comicsUrl
        case .Some(Segues.EventsSegue):
            controller.hqResourceType = HQResourceType.Events
            controller.hqResourceUrl = self.character.eventsUrl
        case .Some(Segues.StoriesSegue):
            controller.hqResourceType = HQResourceType.Stories
            controller.hqResourceUrl = self.character.storiesUrl
        case .Some(Segues.SeriesSegue):
            controller.hqResourceType = HQResourceType.Series
            controller.hqResourceUrl = self.character.seriesUrl
        default:
            break
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.navigationController?.navigationBar.alpha = 0.7
    }
    

}
