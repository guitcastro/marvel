//
//  MarvelCarouselViewController.swift
//  Marvel
//
//  Created by Guilherme on 5/17/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit
import iCarousel

class MarvelCarouselViewController : UIViewController, iCarouselDataSource, iCarouselDelegate
{
    var items: [HQResource]!
    @IBOutlet var carousel : iCarousel!
    @IBOutlet weak var pager: UILabel!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        carousel.type = .Linear
        carousel.reloadData()
        self.carouselCurrentItemIndexDidChange(self.carousel)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    @IBAction func didTouchCloseButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    // MARK: - iCarousel
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return items.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView: ComicBookView
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            let nib = NSBundle.mainBundle().loadNibNamed("ComicBookView", owner: self, options: nil)
            itemView = nib.first as! ComicBookView
            
        }
        else
        {
            itemView = view as! ComicBookView
        }
        
        itemView.bind(items[index])
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        self.pager.text = "\(carousel.currentItemIndex + 1)/\(self.items.count)"
    }
}