//
//  ImageView+downloadAndShowImage.swift
//  Marvel
//
//  Created by Guilherme on 5/16/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import AlamofireImage

extension UIImageView {
    
    func downloadAndShow(imageUrl: String?) {
        self.hidden = true
        let fallbackImage = UIImage(named: "placeholder")
        
        guard let _ = imageUrl else {
            self.image = fallbackImage
            self.hidden = false
            return
        }
        guard let url = NSURL(string: imageUrl!) else {
            self.image = fallbackImage
            self.hidden = false
            return
        }
        self.af_setImageWithURL(url, completion: { _ in self.hidden = false })
    }
    
}