//
//  Colors.swift
//  Marvel
//
//  Created by Guilherme on 5/14/16.
//  Copyright © 2016 Guilherme Castro. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    static let MarvelRed = UIColor(red: 231/255.0, green: 0, blue: 17/255.0, alpha: 1)
    static let MarvelDarkNavigationBarColor = UIColor(red: 0x28/255.0, green: 0x2C/255.0, blue: 0x30/255.0, alpha: 1)

}